/**
 * This shows you how to use the HTTP line protocol
 */

const request = require('request-promise-native');
const INFLUX_WRITE_URL = `http://192.168.0.2:8086/write?db=demo`;

(async () => {
    try {
        let temp = Math.floor(Math.random() * 10 + 20);     // rand between 20-30
        let now = new Date().getTime();
        let insertQuery=`sensor,id=2,type=WaterTempC temperature=${temp} ${now * 1e6}`;
        console.log(insertQuery);
        const resp = await request.post({url: INFLUX_WRITE_URL, body: insertQuery});
        console.log(resp);
    } catch (e) {
        console.warn(e);
    }
})();

