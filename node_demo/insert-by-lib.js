/**
 * This shows you how to use the library
 */
const Influx = require('influx');
let influx = new Influx.InfluxDB({
    host: '192.168.0.2',
    database: 'demo',
});


(async () => {
    try {
        let query = `select mean(temperature) from sensor group by id, time(5m)`;
        let result = await influx.query(query);
        for (let r of result) {
            console.log(`${r.time}: ${r.mean}`);
        }
    } catch (e) {
        console.warn(e);
    }
})();

